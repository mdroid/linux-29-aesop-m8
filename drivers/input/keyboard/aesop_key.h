/*
 *	
 * Aesop key-pad header file (charles.park) 
 *
 */

#ifndef	_AESOP_KEYPAD_H_
#define	_AESOP_KEYPAD_H_

	#if defined(CONFIG_CPU_S3C6400) || defined (CONFIG_CPU_S3C6410)

		#define TRUE 				1
		#define FALSE 				0
		
		#define	KEY_PRESS			1
		#define	KEY_RELEASE			0
		
		// Debug message enable flag
		//#define	DEBUG_MSG			

		//
		// Special key define
		//
		#define	SPECIAL_KEY_MASK	(KEY_MAX + 1)			// KEY_MAX = 0x1FF
		#define	KEY_AT				(SPECIAL_KEY_MASK + 0)	// KEY_SHIFT + KEY_2 (2 bytes send)
		
		//
		// Aesop Mainboard key matrix define
		//
		#define	MAX_MAIN_KEYPAD_CNT	5
		#define	MAIN_KEYPAD_MASK	0x1F
		
		
		#if 0 // ghcstop fix for android
		int AesopMainKeyPad_Keycode[MAX_MAIN_KEYPAD_CNT] = {
			KEY_UP,	KEY_DOWN, KEY_ENTER, KEY_LEFT, KEY_RIGHT
		};
		#else
			#if 0
				#define KEY_HOME        102                        
				#define KEY_BACK        158 /* AC Back */
				#define KEY_MENU        139 /* Menu (show menu) */ 
				#define KEY_SEND        231 /* AC Send */
				#define KEY_END         107
			#endif


			int AesopMainKeyPad_Keycode[MAX_MAIN_KEYPAD_CNT] = {
				KEY_BACK, KEY_HOME, KEY_MENU, KEY_END, KEY_SEND
			};
		#endif

		//
		// Aesop Baseboard key matrix define
		//
		#define KEYPAD_COLUMNS	6
		#define KEYPAD_ROWS		8
		#define MAX_KEYPAD_CNT	(KEYPAD_COLUMNS * KEYPAD_ROWS)
		
		int AesopKeyPad_Keycode[MAX_KEYPAD_CNT] = {
/* COLUMNS 0 */	KEY_1,			KEY_2, 			KEY_3, 		KEY_4, 		KEY_5, 		KEY_6, 			KEY_7, 			KEY_8,
/* COLUMNS 1 */	KEY_9, 			KEY_0, 			KEY_Q, 		KEY_W, 		KEY_E, 		KEY_R, 			KEY_T, 			KEY_Y,
/* COLUMNS 2 */	KEY_U, 			KEY_I, 			KEY_O, 		KEY_P, 		KEY_A, 		KEY_S, 			KEY_D, 			KEY_F,
/* COLUMNS 3 */	KEY_G, 			KEY_H, 			KEY_J, 		KEY_K, 		KEY_L, 		KEY_BACKSPACE, 	KEY_MENU, 		KEY_Z,
/* COLUMNS 4 */	KEY_X, 			KEY_C, 			KEY_V, 		KEY_B, 		KEY_N, 		KEY_M, 			KEY_DOT, 		KEY_ENTER,
/* COLUMNS 5 */	KEY_LEFTSHIFT, 	KEY_LEFTALT, 	KEY_ZOOM, 	KEY_AT, 	KEY_SPACE, 	KEY_COMMA, 		KEY_RIGHTALT, 	KEY_RIGHTSHIFT
		};

		//
		// GPM Port define (Mainboard keypad)
		//
		#define	GPMCON				(*(unsigned long *)S3C_GPMCON)
		#define	GPMPUD				(*(unsigned long *)S3C_GPMPU)
		#define	GPMDAT				(*(unsigned long *)S3C_GPMDAT)

		// Initialize GPM.0 ~ GPM.5 Input (Main Key Input)
		#define	GPM_CON_DATA		0x00000000
		// Initialize GPM.0 ~ GPM.5 Pull-up Enable (Main Key Input)
		#define	GPM_PUD_DATA		0x000002AA

		// GPM Init Function		
		#define	GPM_INIT()			{	GPMCON = GPM_CON_DATA;	GPMPUD = (GPMPUD & 0xFFFFFC00) | GPM_PUD_DATA;	}
		#define	GET_MAIN_KEYPAD_DATA()		((~GPMDAT) & MAIN_KEYPAD_MASK)		// reverse control

		//
		// GPK Port define		
		//
		#define	GPKCON0				(*(unsigned long *)S3C_GPK0CON)
		#define	GPKCON1				(*(unsigned long *)S3C_GPK1CON)
		#define	GPKPUD				(*(unsigned long *)S3C_GPKPU)
		#define	GPKDAT				(*(unsigned long *)S3C_GPKDAT)

		// Initialize GPK.3 ~ GPK.7 output (LED)
		#define	GPK_CON0_DATA		0x11111100
		// Initialize GPK.8 ~ GPK.15 Input (Key ROW)
		#define	GPK_CON1_DATA		0x00000000
		// Initialize GPK.8 ~ GPK.15 Pull-up Enable (Key ROW)
		#define	GPK_PUD_DATA		0xAAAA0000

		// GPK Init Function		
		#define	GPK_INIT()			{	GPKCON0 = (GPKCON0 & 0x000000FF) | GPK_CON0_DATA;	GPKCON1 = GPK_CON1_DATA;	GPKPUD = (GPKPUD & 0x0000FFFF) | GPK_PUD_DATA;	}

		// Keypad data read function
		#define	GET_KEYPAD_ROW()	((~(GPKDAT >> 8)) & 0x000000FF)		// reverse control
		
		// LED 1,2,3 (Value = 0 ~ 7)
		#define	SET_MAIN_LED(led)	(GPKDAT = (GPKDAT & (~0x38)) | ((~led << 3) & 0x38))
		
		//
		// GPL Port define
		//
		#define	GPLCON0				(*(unsigned long *)S3C_GPL0CON)
		#define	GPLCON1				(*(unsigned long *)S3C_GPL1CON)
		#define	GPLPUD				(*(unsigned long *)S3C_GPLPU)
		#define	GPLDAT				(*(unsigned long *)S3C_GPLDAT)
		
		// Keypad column data set function
//		#define	SET_KEYPAD_COL(col)	GPLDAT = ((GPLDAT & 0xFFFFFF00) | ((~col) & 0x000000FF))	// reverse control
		#define	SET_KEYPAD_COL(col)	GPLDAT = ((GPLDAT & 0xFFFFFF00) | ((~(0x01<<col)) & 0x000000FF))	// reverse control

		// Initialize GPL.0 ~ GPL.7 Output (Key COL)
		#define	GPL_CON0_DATA		0x11111111;

		// Initialize GPL.0 ~ GPL.7 Pull-up Enable (Key COL)
		#define	GPL_PUD_DATA		0x0000AAAA

		// GPL Init Function		
		#define	GPL_INIT()			{	GPLCON0 = GPL_CON0_DATA;	GPLPUD = (GPLPUD & 0xFFFF0000) | GPL_PUD_DATA;	}


		#if	defined(DEBUG_MSG)
			#if 0 // ghcstop fix for android
				char AesopMainKeyPad_KeycodeStr[MAX_MAIN_KEYPAD_CNT][20] = {
					"Main KEY_UP\n", "Main KEY_DOWN\n", "Main KEY_ENTER\n", "Main KEY_LEFT\n", "Main KEY_RIGHT\n"
				};
			#else
				char AesopMainKeyPad_KeycodeStr[MAX_MAIN_KEYPAD_CNT][20] = {
					"Main KEY_BACK\n", "Main KEY_HOME\n", "Main KEY_MENU\n", "Main KEY_END\n", "Main KEY_SEND\n"
				};
			#endif			
			
			char AesopKeyPad_KeycodeStr[MAX_KEYPAD_CNT][20] = {
		/* COLUMNS 0 */	"KEY_1\n",			"KEY_2\n", 			"KEY_3\n", 		"KEY_4\n", 		"KEY_5\n", 		"KEY_6\n", 			"KEY_7\n", 			"KEY_8\n",
		/* COLUMNS 1 */	"KEY_9\n", 			"KEY_0\n", 			"KEY_Q\n", 		"KEY_W\n", 		"KEY_E\n", 		"KEY_R\n", 			"KEY_T\n", 			"KEY_Y\n",
		/* COLUMNS 2 */	"KEY_U\n", 			"KEY_I\n", 			"KEY_O\n", 		"KEY_P\n", 		"KEY_A\n", 		"KEY_S\n", 			"KEY_D\n", 			"KEY_F\n",
		/* COLUMNS 3 */	"KEY_G\n", 			"KEY_H\n", 			"KEY_J\n", 		"KEY_K\n", 		"KEY_L\n", 		"KEY_BACKSPACE\n", 	"KEY_MENU\n", 		"KEY_Z\n",
		/* COLUMNS 4 */	"KEY_X\n", 			"KEY_C\n", 			"KEY_V\n", 		"KEY_B\n", 		"KEY_N\n", 		"KEY_M\n", 			"KEY_DOT\n", 		"KEY_ENTER\n",
		/* COLUMNS 5 */	"KEY_LEFTSHIFT\n", 	"KEY_LEFTALT\n", 	"KEY_ZOOM\n", 	"KEY_AT\n", 	"KEY_SPACE\n", 	"KEY_COMMA\n", 		"KEY_RIGHTALT\n", 	"KEY_RIGHTSHIFT\n"
			};
				
		#endif	// DEBUG_MSG
		
	#else
		#error "Not supported S3C Configuration!!"
	#endif
#endif		/* _AESOP_KEYPAD_H_*/
