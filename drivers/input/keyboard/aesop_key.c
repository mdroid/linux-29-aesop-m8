//[*]--------------------------------------------------------------------------------------------------[*]

/*
 *
 * Aesop keypad driver (charles.park)
 *
 */

//[*]--------------------------------------------------------------------------------------------------[*]
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/input.h>
#include <linux/platform_device.h>
#include <linux/miscdevice.h>
#include <linux/device.h>

//[*]--------------------------------------------------------------------------------------------------[*]
#include <asm/io.h>
#include <asm/delay.h>
#include <asm/irq.h>

#include <mach/map.h>
#include <mach/gpio.h>
#include <plat/gpio-cfg.h>
#include <plat/regs-oldstyle-gpio.h>
#include "aesop_key.h"

//[*]--------------------------------------------------------------------------------------------------[*]
#define DEVICE_NAME "aesop-keypad"

//[*]--------------------------------------------------------------------------------------------------[*]
static struct timer_list 	keypad_timer;
static struct input_dev		*aesop_keypad;

//[*]--------------------------------------------------------------------------------------------------[*]
static void				generate_special_keycode		(unsigned int special_key_code, unsigned char status);
static void				generate_keycode				(unsigned char prev_key, unsigned char cur_key, int *key_table, unsigned char col);
static void				keypad_timer_handler			(unsigned long data);

static int              aesop_keypad_open               (struct input_dev *dev);
static void             aesop_keypad_close              (struct input_dev *dev);

static void             aesop_keypad_release_device     (struct device *dev);
static int              aesop_keypad_resume             (struct device *dev);
static int              aesop_keypad_suspend            (struct device *dev, pm_message_t state);

static int __devinit    aesop_keypad_probe              (struct device *pdev);
static int __devexit    aesop_keypad_remove             (struct device *pdev);

static int __init       aesop_keypad_init               (void);
static void __exit      aesop_keypad_exit               (void);

//[*]--------------------------------------------------------------------------------------------------[*]
struct platform_device aesop_platform_device_driver = {
        .name           = DEVICE_NAME,
        .id             = 0,
        .num_resources  = 0,
        .dev    = {
                .release        = aesop_keypad_release_device,
        },
};

//[*]--------------------------------------------------------------------------------------------------[*]
struct device_driver aesop_device_driver = {
        .owner          = THIS_MODULE,
        .name           = DEVICE_NAME,
        .bus            = &platform_bus_type,
        .probe          = aesop_keypad_probe,
        .remove         = __devexit_p(aesop_keypad_remove),
        .suspend        = aesop_keypad_suspend,
        .resume         = aesop_keypad_resume,
};

//[*]--------------------------------------------------------------------------------------------------[*]
module_init(aesop_keypad_init);
module_exit(aesop_keypad_exit);

//[*]--------------------------------------------------------------------------------------------------[*]
MODULE_AUTHOR("Hard-Kernel");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("KeyPad interface for Aesop-6410 Board");

//[*]--------------------------------------------------------------------------------------------------[*]
static void	generate_special_keycode(unsigned int special_key_code, unsigned char status)
{
	switch(special_key_code)	{
		case	KEY_AT:
			if(status)	{	// key press
				input_report_key(aesop_keypad, KEY_LEFTSHIFT, status);
				input_report_key(aesop_keypad, KEY_2, status);
			}
			else	{
				input_report_key(aesop_keypad, KEY_2, status);
				input_report_key(aesop_keypad, KEY_LEFTSHIFT, status);
			}
			break;
		
		default	:
			printk("unkown key code!!\n");
			break;
	}
} 	
//[*]--------------------------------------------------------------------------------------------------[*]
static void	generate_keycode(unsigned char prev_key, unsigned char cur_key, int *key_table, unsigned char col)
{
	unsigned char 	press_key, release_key, i;
	
	press_key	= (cur_key ^ prev_key) & cur_key;
	release_key	= (cur_key ^ prev_key) & prev_key;
	
	i = 0;
	while(press_key)	{
		if(press_key & 0x01)	{
			if(key_table[col * 8 + i] & SPECIAL_KEY_MASK)	{
				generate_special_keycode(key_table[col * 8 + i], KEY_PRESS);
				#if defined(DEBUG_MSG)
					printk("Special key press!!\n");
				#endif
			}
			else
			{
				//printk("pressed keycode = %d\n", key_table[col * 8 + i] );	
				input_report_key(aesop_keypad, key_table[col * 8 + i], KEY_PRESS);
			}
		}
		i++;	press_key >>= 1;
	}
	
	i = 0;
	while(release_key)	{
		if(release_key & 0x01)	{
			if(key_table[col * 8 + i] & SPECIAL_KEY_MASK)	{
				generate_special_keycode(key_table[col * 8 + i], KEY_RELEASE);
				#if defined(DEBUG_MSG)
					printk("Special key release!!\n");
				#endif
			}
			else
			{
				//printk("released keycode = %d\n", key_table[col * 8 + i] );	
				input_report_key(aesop_keypad, key_table[col * 8 + i], KEY_RELEASE);
			}
		}
		i++;	release_key >>= 1;
	}
}
//[*]--------------------------------------------------------------------------------------------------[*]
#if defined(DEBUG_MSG)
	static void debug_keycode_printf(unsigned char prev_key, unsigned char cur_key, char *key_table, unsigned char col)
	{
		unsigned char 	press_key, release_key, i;
		
		press_key	= (cur_key ^ prev_key) & cur_key;
		release_key	= (cur_key ^ prev_key) & prev_key;
		
		i = 0;
		while(press_key)	{
			if(press_key & 0x01)	printk("PRESS KEY : %s", (char *)&key_table[col * 8 * 20 + i * 20]);
			i++;					press_key >>= 1;
		}
		
		i = 0;
		while(release_key)	{
			if(release_key & 0x01)	printk("RELEASE KEY : %s", (char *)&key_table[col * 8 * 20 + i * 20]);
			i++;					release_key >>= 1;
		}
	}
#endif

//[*]--------------------------------------------------------------------------------------------------[*]
static void	keypad_timer_handler(unsigned long data)
{
	static	unsigned char	columns = 0;
	static	unsigned char	prev_main_keypad_data = 0, cur_main_keypad_data = 0;
	static	unsigned char	prev_keypad_data[KEYPAD_COLUMNS] = { 0, 0, 0, 0, 0, 0 };
	static	unsigned char	cur_keypad_data[KEYPAD_COLUMNS] = { 0, 0, 0, 0, 0, 0 };
			unsigned char	count;
	
	static	unsigned int	LedTestCount = 0;

	// get current column data
	cur_keypad_data[columns] = GET_KEYPAD_ROW();
	
	// Set next column setting
	columns ++;		columns %= KEYPAD_COLUMNS;		SET_KEYPAD_COL(columns);
	
	if(!columns)	{
		cur_main_keypad_data = GET_MAIN_KEYPAD_DATA();
		
		if(prev_main_keypad_data != cur_main_keypad_data)	{
			generate_keycode(prev_main_keypad_data, cur_main_keypad_data, &AesopMainKeyPad_Keycode[0], 0);
			#if defined(DEBUG_MSG)
				debug_keycode_printf(prev_main_keypad_data, cur_main_keypad_data, &AesopMainKeyPad_KeycodeStr[0][0], 0);
			#endif
			prev_main_keypad_data = cur_main_keypad_data;
		}
		
		for(count = 0; count < KEYPAD_COLUMNS; count++)	{
			if(prev_keypad_data[count] != cur_keypad_data[count])	{
				generate_keycode(prev_keypad_data[count], cur_keypad_data[count], &AesopKeyPad_Keycode[0], count);
				#if defined(DEBUG_MSG)
					debug_keycode_printf(prev_keypad_data[count], cur_keypad_data[count], &AesopKeyPad_KeycodeStr[0][0], count);
				#endif
				prev_keypad_data[count] = cur_keypad_data[count];
			}
		}

		SET_MAIN_LED(LedTestCount++ % 8);	// Test LED
	}
	// Kernel Timer restart
	mod_timer(&keypad_timer,jiffies + HZ/100);
}

//[*]--------------------------------------------------------------------------------------------------[*]
static int	aesop_keypad_open(struct input_dev *dev)
{
	#if	defined(DEBUG_MSG)
		printk("%s\n", __FUNCTION__);
	#endif
	
	return	0;
}

//[*]--------------------------------------------------------------------------------------------------[*]
static void	aesop_keypad_close(struct input_dev *dev)
{
	#if	defined(DEBUG_MSG)
		printk("%s\n", __FUNCTION__);
	#endif
}

//[*]--------------------------------------------------------------------------------------------------[*]
static void	aesop_keypad_release_device(struct device *dev)
{
	#if	defined(DEBUG_MSG)
		printk("%s\n", __FUNCTION__);
	#endif
}

//[*]--------------------------------------------------------------------------------------------------[*]
static int	aesop_keypad_resume(struct device *dev)
{
	#if	defined(DEBUG_MSG)
		printk("%s\n", __FUNCTION__);
	#endif
	
    return  0;
}

//[*]--------------------------------------------------------------------------------------------------[*]
static int	aesop_keypad_suspend(struct device *dev, pm_message_t state)
{
	#if	defined(DEBUG_MSG)
		printk("%s\n", __FUNCTION__);
	#endif
	
    return  0;
}

//[*]--------------------------------------------------------------------------------------------------[*]
static int __devinit    aesop_keypad_probe(struct device *pdev)
{
    int 	key, code;

	aesop_keypad = input_allocate_device();
	
    if(!(aesop_keypad))	{
		printk("error!!! no memory!!\n");
		return -ENOMEM;
    }

	GPK_INIT();		GPL_INIT();		GPM_INIT();		SET_KEYPAD_COL(0);	//SET_KEYPAD_COL(0);

	set_bit(EV_KEY, aesop_keypad->evbit);
//	set_bit(EV_REP, aesop_keypad->evbit);	// Repeat Key

	for(key = 0; key < MAX_KEYPAD_CNT; key++){
		code = AesopKeyPad_Keycode[key];
		if(code<=0)
			continue;
		set_bit(code & KEY_MAX, aesop_keypad->keybit);
	}
	

	//printk(".........ggg P=======================\n");

	for(key = 0; key < MAX_MAIN_KEYPAD_CNT; key++){
		code = AesopMainKeyPad_Keycode[key];
		//printk(".........ggg: %d\n", code);
		if(code<=0)
			continue;
		set_bit(code & KEY_MAX, aesop_keypad->keybit);
	}
	//printk(".........ggg A=======================\n");

	aesop_keypad->name 	= DEVICE_NAME;
	aesop_keypad->phys 	= "aesop-keypad/input0";
    aesop_keypad->open 	= aesop_keypad_open;
    aesop_keypad->close	= aesop_keypad_close;
	
	aesop_keypad->id.bustype	= BUS_HOST;
	aesop_keypad->id.vendor 	= 0x0001;
	aesop_keypad->id.product 	= 0x0001;
	aesop_keypad->id.version 	= 0x0001;

	aesop_keypad->keycode = AesopKeyPad_Keycode;
	
	if(input_register_device(aesop_keypad))	{
		printk("Aesop keypad input register device fail!!\n");

		input_free_device(aesop_keypad);		return	-ENODEV;
	}

	/* Scan timer init */
	init_timer(&keypad_timer);
	keypad_timer.function = keypad_timer_handler;

	keypad_timer.expires = jiffies + (HZ/100);
	add_timer(&keypad_timer);

	printk("Aesop Keypad Initialized!!\n");

    return 0;
}

//[*]--------------------------------------------------------------------------------------------------[*]
static int __devexit    aesop_keypad_remove(struct device *pdev)
{
	input_unregister_device(aesop_keypad);
	
	del_timer(&keypad_timer);
	
	#if	defined(DEBUG_MSG)
		printk("%s\n", __FUNCTION__);
	#endif
	
	return  0;
}

//[*]--------------------------------------------------------------------------------------------------[*]
static int __init	aesop_keypad_init(void)
{
	int ret = driver_register(&aesop_device_driver);
	
	#if	defined(DEBUG_MSG)
		printk("%s\n", __FUNCTION__);
	#endif
	
	if(!ret)        {
		ret = platform_device_register(&aesop_platform_device_driver);
		
		#if	defined(DEBUG_MSG)
			printk("platform_driver_register %d \n", ret);
		#endif
		
		if(ret)	driver_unregister(&aesop_device_driver);
	}
	return ret;
}

//[*]--------------------------------------------------------------------------------------------------[*]
static void __exit	aesop_keypad_exit(void)
{
	#if	defined(DEBUG_MSG)
		printk("%s\n",__FUNCTION__);
	#endif
	
	platform_device_unregister(&aesop_platform_device_driver);
	driver_unregister(&aesop_device_driver);
}

//[*]--------------------------------------------------------------------------------------------------[*]
