/*
 * drivers/video/s3c/s3cfb_lte480wv.c
 *
 * $Id: s3cfb_ltv350qv.c,v 1.1 2008/11/17 11:12:08 jsgood Exp $
 *
 * Copyright (C) 2008 Jinsung Yang <jsgood.yang@samsung.com>
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file COPYING in the main directory of this archive for
 * more details.
 *
 *	S3C Frame Buffer Driver
 *	based on skeletonfb.c, sa1100fb.h, s3c2410fb.c
 */

#include <linux/wait.h>
#include <linux/fb.h>
#include <linux/delay.h>
#include <linux/platform_device.h>

#include <plat/regs-lcd.h>

#include <mach/gpio.h>
#include <plat/gpio-cfg.h>

#include <plat/regs-oldstyle-gpio.h> // ghcstop add

#include "s3cfb.h"

#define S3CFB_SPI_CH		0	/* spi channel for module init */

#define S3CFB_HFP		10	/* front porch */
#define S3CFB_HSW		2	/* hsync width */
#define S3CFB_HBP		6	/* back porch */

#define S3CFB_VFP		3	/* front porch */
#define S3CFB_VSW		2	/* vsync width */
#define S3CFB_VBP		3	/* back porch */

#define S3CFB_HRES		320	/* horizon pixel  x resolition */
#define S3CFB_VRES		480	/* line cnt       y resolution */

#define S3CFB_HRES_VIRTUAL	320	/* horizon pixel  x resolition */
#define S3CFB_VRES_VIRTUAL	(480*2)	/* line cnt       y resolution */

#define S3CFB_HRES_OSD		320	/* horizon pixel  x resolition */
#define S3CFB_VRES_OSD		480	/* line cnt       y resolution */

#define S3CFB_VFRAME_FREQ     	60	/* frame rate freq */

#define S3CFB_PIXEL_CLOCK	(S3CFB_VFRAME_FREQ * (S3CFB_HFP + S3CFB_HSW + S3CFB_HBP + S3CFB_HRES) * (S3CFB_VFP + S3CFB_VSW + S3CFB_VBP + S3CFB_VRES))

static void s3cfb_set_fimd_info(void)
{
	s3cfb_fimd.vidcon1 = S3C_VIDCON1_IHSYNC_INVERT | S3C_VIDCON1_IVSYNC_INVERT | S3C_VIDCON1_IVDEN_NORMAL;
	s3cfb_fimd.vidtcon0 = S3C_VIDTCON0_VBPD(S3CFB_VBP - 1) | S3C_VIDTCON0_VFPD(S3CFB_VFP - 1) | S3C_VIDTCON0_VSPW(S3CFB_VSW - 1);
	s3cfb_fimd.vidtcon1 = S3C_VIDTCON1_HBPD(S3CFB_HBP - 1) | S3C_VIDTCON1_HFPD(S3CFB_HFP - 1) | S3C_VIDTCON1_HSPW(S3CFB_HSW - 1);
	s3cfb_fimd.vidtcon2 = S3C_VIDTCON2_LINEVAL(S3CFB_VRES - 1) | S3C_VIDTCON2_HOZVAL(S3CFB_HRES - 1);

	s3cfb_fimd.vidosd0a = S3C_VIDOSDxA_OSD_LTX_F(0) | S3C_VIDOSDxA_OSD_LTY_F(0);
	s3cfb_fimd.vidosd0b = S3C_VIDOSDxB_OSD_RBX_F(S3CFB_HRES - 1) | S3C_VIDOSDxB_OSD_RBY_F(S3CFB_VRES - 1);

	s3cfb_fimd.vidosd1a = S3C_VIDOSDxA_OSD_LTX_F(0) | S3C_VIDOSDxA_OSD_LTY_F(0);
	s3cfb_fimd.vidosd1b = S3C_VIDOSDxB_OSD_RBX_F(S3CFB_HRES_OSD - 1) | S3C_VIDOSDxB_OSD_RBY_F(S3CFB_VRES_OSD - 1);

	s3cfb_fimd.width = S3CFB_HRES;
	s3cfb_fimd.height = S3CFB_VRES;
	s3cfb_fimd.xres = S3CFB_HRES;
	s3cfb_fimd.yres = S3CFB_VRES;

#if defined(CONFIG_FB_S3C_VIRTUAL_SCREEN)
	s3cfb_fimd.xres_virtual = S3CFB_HRES_VIRTUAL;
	s3cfb_fimd.yres_virtual = S3CFB_VRES_VIRTUAL;
#else
	s3cfb_fimd.xres_virtual = S3CFB_HRES;
	s3cfb_fimd.yres_virtual = S3CFB_VRES;
#endif

	s3cfb_fimd.osd_width = S3CFB_HRES_OSD;
	s3cfb_fimd.osd_height = S3CFB_VRES_OSD;
	s3cfb_fimd.osd_xres = S3CFB_HRES_OSD;
	s3cfb_fimd.osd_yres = S3CFB_VRES_OSD;

	s3cfb_fimd.osd_xres_virtual = S3CFB_HRES_OSD;
	s3cfb_fimd.osd_yres_virtual = S3CFB_VRES_OSD;

     	s3cfb_fimd.pixclock = S3CFB_PIXEL_CLOCK;

	s3cfb_fimd.hsync_len = S3CFB_HSW;
	s3cfb_fimd.vsync_len = S3CFB_VSW;
	s3cfb_fimd.left_margin = S3CFB_HFP;
	s3cfb_fimd.upper_margin = S3CFB_VFP;
	s3cfb_fimd.right_margin = S3CFB_HBP;
	s3cfb_fimd.lower_margin = S3CFB_VBP;
}

void s3cfb_spi_write(int address, int data)
{
	unsigned int delay = 50;
	unsigned char dev_id = 0x1d;
	int i;

	s3cfb_spi_lcd_den(S3CFB_SPI_CH, 1);
	s3cfb_spi_lcd_dclk(S3CFB_SPI_CH, 1);
	s3cfb_spi_lcd_dseri(S3CFB_SPI_CH, 1);
	udelay(delay);

	s3cfb_spi_lcd_den(S3CFB_SPI_CH, 0);
	udelay(delay);

	for (i = 5; i >= 0; i--) {
		s3cfb_spi_lcd_dclk(S3CFB_SPI_CH, 0);

		if ((dev_id >> i) & 0x1)
			s3cfb_spi_lcd_dseri(S3CFB_SPI_CH, 1);
		else
			s3cfb_spi_lcd_dseri(S3CFB_SPI_CH, 0);

		udelay(delay);

		s3cfb_spi_lcd_dclk(S3CFB_SPI_CH, 1);
		udelay(delay);
	}

	s3cfb_spi_lcd_dclk(S3CFB_SPI_CH, 0);
	s3cfb_spi_lcd_dseri(S3CFB_SPI_CH, 0);
	udelay(delay);

	s3cfb_spi_lcd_dclk(S3CFB_SPI_CH, 1);
	udelay(delay);

	s3cfb_spi_lcd_dclk(S3CFB_SPI_CH, 0);
	s3cfb_spi_lcd_dseri(S3CFB_SPI_CH, 0);
	udelay(delay);

	s3cfb_spi_lcd_dclk(S3CFB_SPI_CH, 1);
	udelay(delay);

	for (i = 15; i >= 0; i--) {
		s3cfb_spi_lcd_dclk(S3CFB_SPI_CH, 0);

		if ((address >> i) & 0x1)
			s3cfb_spi_lcd_dseri(S3CFB_SPI_CH, 1);
		else
			s3cfb_spi_lcd_dseri(S3CFB_SPI_CH, 0);

		udelay(delay);

		s3cfb_spi_lcd_dclk(S3CFB_SPI_CH, 1);
		udelay(delay);
	}

	s3cfb_spi_lcd_dseri(S3CFB_SPI_CH, 1);
	udelay(delay);

	s3cfb_spi_lcd_den(S3CFB_SPI_CH, 1);
	udelay(delay * 10);

	s3cfb_spi_lcd_den(S3CFB_SPI_CH, 0);
	udelay(delay);

	for (i = 5; i >= 0; i--) {
		s3cfb_spi_lcd_dclk(S3CFB_SPI_CH, 0);

		if ((dev_id >> i) & 0x1)
			s3cfb_spi_lcd_dseri(S3CFB_SPI_CH, 1);
		else
			s3cfb_spi_lcd_dseri(S3CFB_SPI_CH, 0);

		udelay(delay);

		s3cfb_spi_lcd_dclk(S3CFB_SPI_CH, 1);
		udelay(delay);

	}

	s3cfb_spi_lcd_dclk(S3CFB_SPI_CH, 0);
	s3cfb_spi_lcd_dseri(S3CFB_SPI_CH, 1);
	udelay(delay);

	s3cfb_spi_lcd_dclk(S3CFB_SPI_CH, 1);
	udelay(delay);

	s3cfb_spi_lcd_dclk(S3CFB_SPI_CH, 0);
	s3cfb_spi_lcd_dseri(S3CFB_SPI_CH, 0);
	udelay(delay);

	s3cfb_spi_lcd_dclk(S3CFB_SPI_CH, 1);
	udelay(delay);

	for (i = 15; i >= 0; i--) {
		s3cfb_spi_lcd_dclk(S3CFB_SPI_CH, 0);

		if ((data >> i) & 0x1)
			s3cfb_spi_lcd_dseri(S3CFB_SPI_CH, 1);
		else
			s3cfb_spi_lcd_dseri(S3CFB_SPI_CH, 0);

		udelay(delay);

		s3cfb_spi_lcd_dclk(S3CFB_SPI_CH, 1);
		udelay(delay);

	}

	s3cfb_spi_lcd_den(S3CFB_SPI_CH, 1);
	udelay(delay);
}

#define FRAME_CYCLE_TIME	25

static void s3cfb_init_ldi(void)
{
	s3cfb_spi_set_lcd_data(S3CFB_SPI_CH);
	mdelay(5);

	s3cfb_spi_lcd_den(S3CFB_SPI_CH, 1);
	s3cfb_spi_lcd_dclk(S3CFB_SPI_CH, 1);
	s3cfb_spi_lcd_dseri(S3CFB_SPI_CH, 1);

	// power on sequence
	s3cfb_spi_write(0x07, 0x0000);
	mdelay(12);
	
	s3cfb_spi_write(0x11, 0x333f);
	s3cfb_spi_write(0x12, 0x0f00);
	s3cfb_spi_write(0x13, 0x7fe2);
	s3cfb_spi_write(0x10, 0x460c);
	mdelay(FRAME_CYCLE_TIME*6);
	
	s3cfb_spi_write(0x12, 0x1663);
	mdelay(FRAME_CYCLE_TIME*5);
	
	s3cfb_spi_write(0x01, 0x0B3B);
	s3cfb_spi_write(0x02, 0x0300);
	s3cfb_spi_write(0x03, 0xC040);		// SYNC without DE Mode
	s3cfb_spi_write(0x08, 0x0004);		// VBP
	s3cfb_spi_write(0x09, 0x0008);		// HBP
	s3cfb_spi_write(0x76, 0x2213);
	s3cfb_spi_write(0x0B, 0x33E0);
	s3cfb_spi_write(0x0C, 0x0020);
	s3cfb_spi_write(0x76, 0x0000);
	s3cfb_spi_write(0x0D, 0x0007);
	s3cfb_spi_write(0x0E, 0x0500);
	s3cfb_spi_write(0x14, 0x0000);
	s3cfb_spi_write(0x15, 0x0803);
	s3cfb_spi_write(0x16, 0x0000);
	s3cfb_spi_write(0x30, 0x0005);
	s3cfb_spi_write(0x31, 0x0300);
	s3cfb_spi_write(0x32, 0x0300);
	s3cfb_spi_write(0x33, 0x0003);
	s3cfb_spi_write(0x34, 0x090C);
	s3cfb_spi_write(0x35, 0x0505);
	s3cfb_spi_write(0x36, 0x0001);
	s3cfb_spi_write(0x37, 0x0303);
	s3cfb_spi_write(0x38, 0x0F09);
	s3cfb_spi_write(0x39, 0x0101);	
	printk("LCD TYPE :: LMS350DF01 Power ON Sequence Done.\n");	

	mdelay(1);

	// display on sequence
	s3cfb_spi_write(0x07, 0x0001);
	mdelay(FRAME_CYCLE_TIME*2);
	s3cfb_spi_write(0x07, 0x0101);
	mdelay(FRAME_CYCLE_TIME*3);
	s3cfb_spi_write(0x76, 0x2213);
	s3cfb_spi_write(0x1c, 0x7770);
	s3cfb_spi_write(0x0b, 0x33e1);
	s3cfb_spi_write(0x76, 0x0000);
	s3cfb_spi_write(0x07, 0x0103);
	printk("LCD TYPE :: LMS350DF01 Display ON SequenceDdone.\n");		
}

void s3cfb_init_hw(void)
{
	printk(KERN_INFO "LCD TYPE :: LMS350DF01 will be initialized\n");

#ifdef CONFIG_MACH_AESOP6410
	// For aESOP-S3C6410 
	
    // LCD_RST ==> High(Off)
	s3c_set_gpio_func(S3C_GPK1, S3C_GPK1_OUTP);
	s3c_gpio_setpin(S3C_GPK1, 1);
	
	// CVPWR BL Power ==> High(On)  
	s3c_set_gpio_func(S3C_GPK0, S3C_GPK0_OUTP);
	s3c_gpio_setpin(S3C_GPK0, 1);
	                     
	// Backlight Control : GPF15 
	s3c_set_gpio_func(S3C_GPF15, S3C_GPF15_OUTP);
	s3c_gpio_setpin(S3C_GPF15, 0); 

    // LCD Reset(On)
	s3c_gpio_setpin(S3C_GPK1, 0); // reset on
	mdelay(100);
	s3c_gpio_setpin(S3C_GPK1, 1); // reset off
	
	s3cfb_set_fimd_info();
	s3cfb_set_gpio();

	if (s3cfb_spi_gpio_request(S3CFB_SPI_CH))
		printk(KERN_ERR "failed to request GPIO for spi-lcd\n");
	else {
		s3cfb_init_ldi();
		s3cfb_spi_gpio_free(S3CFB_SPI_CH);
	}
#endif
}

